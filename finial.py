import argparse
import paho.mqtt.client as mqtt
import logging
import json
import sys

from influxdb import InfluxDBClient

def on_connect(client, userdata, rc):
    print("connected with result code" + str(rc))
    client.subscribe("/json")

def on_message(client, userdata, msg):
    print str(msg.payload)
    d = json.loads(msg.payload.decode('utf-8'))
    print d
    body = [
        {
            "measurement":"temp_humi",
            "tags":{
                "host": "server1",
                "region": "testroom",
            },
            "fields": {
                "temps":float(d["temp"]), "humi":float(d["hum"])
            }
        }
    ]
    db = InfluxDBClient('localhost', 8086, 'root', 'root', 'example')
    db.write_points(body)
    result = db.query('select temp,hum from temp_humi;')
    print("Result: {0}".format(result))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect("192.168.1.8", 1883, 60)

client.loop_forever()
