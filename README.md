# Y2L Sensor Prototype

## Introduction

Arduino -> mqtt Broker(mosquitto) -> InfluxDB(DataBase) -> Grafana.

 - Temperature sensor is AM2301(DHT21).
 - language is python and arduino sketch.

## Requirements

Arduino(version 1.6.12)

 1. PubsubClient (#include<PubSubClient.h>)
 2. LiquidCrystal - depend on your LCD shield (#include<LCD.h> & #include<LiquidCrystal_I2C.h>)
 3. DHT (#include"DHT.h") - always define Sensor depend on your sensor

python(version 2.7.12/3.5.2)

+
need influxdb library - https://github.com/influxdata/influxdb-python