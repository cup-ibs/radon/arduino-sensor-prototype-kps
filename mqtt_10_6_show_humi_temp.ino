/*
 Basic MQTT example with Authentication

  - connects to an MQTT server, providing username
    and password
  - publishes "hello world" to the topic "outTopic"
  - subscribes to the topic "inTopic"
*/
#include<Wire.h>
#include<LCD.h>
#include<LiquidCrystal_I2C.h>
#include"DHT.h"
#include<math.h>
#include<SPI.h>
#include<Ethernet.h>
#include<PubSubClient.h> /*mqtt*/

/*temperature, huniduty sensor define*/
#define DHTPIN 2
#define DHTTYPE DHT21
DHT dht(DHTPIN, DHTTYPE);

/*LCD*/
LiquidCrystal_I2C lcd(0x27,2,1,0,4,5,6,7);

// Update these with values suitable for your network.
byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };
IPAddress ip(192, 168, 1, 177);
IPAddress server(192, 168, 1, 8);
char servername[] = "iot.eclipse.org";
char clientStr[] = "arduinoclient";
char topicStr[] = "/json";

void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
}

EthernetClient ethClient;
PubSubClient client(server, 1883, callback, ethClient);

void setup()
{
  Serial.begin(9600);
  /*lcd begin*/
  lcd.begin(16,2);
  lcd.setBacklightPin(3, POSITIVE);
  lcd.setBacklight(HIGH);
  lcd.home();
  
  Ethernet.begin(mac, ip);
  // Note - the default maximum packet size is 128 bytes. If the
  // combined length of clientId, username and password exceed this,
  // you will need to increase the value of MQTT_MAX_PACKET_SIZE in
  // PubSubClient.h
  client.setServer(server, 1883);
  client.setCallback(callback);
    
    /*DHT begin*/
   dht.begin();
  
}

void loop() {
  float temp, hum;
  String json;
  char jsonStr[200];
  temp = dht.readTemperature();
  hum = dht.readHumidity();

  char tChar[10];
  char hChar[10];
  dtostrf(temp, 4, 2, tChar);
  dtostrf(hum, 4, 2, hChar);

  if (!client.connected()) {
    Serial.print("Trying to connect to: ");
    Serial.println(clientStr);
    client.connect(clientStr);
  }
  if (client.connected() ) {

    client.publish("/temperature", tChar);
    client.publish("/humidity", hChar);
    
    json = "{ \"temp\": ";
    json += temp;
    json += ", \"hum\": ";
    json += hum;
    json += " }";
    json.toCharArray(jsonStr, 200);

    boolean pubresult = client.publish(topicStr,jsonStr);
    Serial.print("attempt to send ");
    Serial.println(jsonStr);
    Serial.print("to ");
    Serial.println(topicStr);
    if (pubresult)
      Serial.println("successfully sent");
    else
      Serial.println("unsuccessfully sent");
  }
  lcd.setCursor(0,0);
  lcd.print("temp : ");
  lcd.println(temp);
  lcd.setCursor(0,1);
  lcd.print("humi : ");
  lcd.println(hum);
  

  delay(1000);
}
